import React, { Component } from "react";
import _ from "lodash";
import logo from "./logo.svg";
import axios from "axios";
import Card from "./components/Card";
import "./App.css";
import moment from "moment";
import coffee from "./Coffee-icon.png";
import { Button } from "./components/Button";

class App extends Component {
  state = {
    tweets: []
  };

  updateTime = () => {
    const { latest } = this.state;
    const now = moment(latest).fromNow();
    this.setState({ now });
  };
  fetchInit = async () => {
    const data = await axios.get("http://51.68.128.220:8080/api/temp");
    if (data) {
      return data;
    }
  };
  async = async () => {
    const response = await this.fetchInit();
    const tweets = response.data.result;
    const sortByLatest = await _.orderBy(tweets, ["id"], ["desc"]);
    console.log(sortByLatest[0].time);
    await this.setState({ tweets: sortByLatest, latest: sortByLatest[0].time });
  };
  componentDidMount() {
    const asyncLoad = async () => {
      await this.async();
      await this.updateTime();
      await this.setState({ timer: setInterval(this.updateTime, 5000) });
    };
    asyncLoad();
  }
  componentWillUnmount() {
    clearInterval(this.state.timer);
  }
  render() {
    const { tweets, now } = this.state;
    return (
      <div className="App">
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <h1 className="App-title">
            Time since last measurement:
            <span
              style={{
                width: 50,
                verticalAlign: "middle",
                display: "inline-block"
              }}
            >
              <img src={coffee} style={{ width: 50 }} />
            </span>{" "}
            {now}
          </h1>
        </header>
        <div
          style={{
            display: "flex",
            padding: "25px 5px ",
            flexDirection: "column",
            alignItems: "center"
          }}
        >
          {tweets.map((tweet, key) => (
            <Card
              key={key}
              message={`${tweet.temperature} degrees Celsius`}
              ts={tweet.time}
            />
          ))}
        </div>
        <Button onClick={this.async} text={"Check again"} />
      </div>
    );
  }
}

export default App;
