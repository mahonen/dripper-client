import React from "react";
import styles from "./styles";
export const Button = ({ text, onClick }) => <a onClick={onClick} style={styles.main}>{text}</a>;
