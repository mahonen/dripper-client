export default {
	main: {
		display: 'inline-block',
		borderRadius: '0.2em',
		margin: '0 0.3em 0.3em 0',
		padding: '0.7em 1.7em',
		boxSizing: 'border-box',
		textDecoration: 'none',
		color: 'white',
		backgroundColor: '#3369ff',
		boxShadow: 'inset 0 -0.6em 1em -0.35em rgba(0,0,0,0.17),inset 0 0.6em 2em -0.3em rgba(255,255,255,0.15),inset 0 0 0em 0.05em rgba(255,255,255,0.12)',
		textAlign: 'center',
		position: 'relative',
		cursor: 'pointer',

	}
}