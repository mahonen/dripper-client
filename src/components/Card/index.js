import React from "react";
import styles from "./styles";
import logo from "./coffee-icon-1.png";
import moment from "moment";

export default ({ message, timestamp }) => (
  <div style={styles.card}>
    <div style={styles.cardHeader}>Jack the Dripper</div>
    <div style={styles.cardMain}>
      <div style={styles.timestamp}>
        {moment(timestamp).format("YYYY-MM-DD HH:mm:ss")}
      </div>
      <img style={styles.icons} src={logo} alt="coffee" />
      <div style={styles.desc}>{message}</div>
    </div>
  </div>
);
