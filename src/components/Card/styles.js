export default {
  card: {
    width: 280,
	backgroundColor:'white',
    border: "1px solid #ef9a9a",
    borderRadius: 4,
    overflow: "hidden",
    display: "flex",
    flexDirection: "column",
    marginBottom: 15
  },
  cardHeader: {
    color: "#d32f2f",
    fontSize: "12px",
    fontWeight: "bold",
    borderBottom: "1px solid #ef9a9a",
    backgroundColor: "#ffebee",
    padding: "5px 10px"
  },
  cardMain: {
    display: "flex",
    flexDirection: "column",
    justifyContent: "center",
    alignItems: "center",
    padding: "10px"
  },
  icons: {
    height: "50px",
    width: "50px"
  },
  desc: {
    color: "#d32f2f",
    fontSize: "14px",
    textAlign: "center",
    padding: "10px 0",
    borderTop: "1px solid #d32f2f",
    marginTop: 10
  },
  timestamp: {
    color: "#d32f2f",
    fontSize: "10px",
    textAlign: "center",
    padding: "5px 0"
  }
};
